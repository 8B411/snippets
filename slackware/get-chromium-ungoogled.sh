#/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

set -e

lftp <<EOF
  open http://www.slackware.com/~alien/slackbuilds/
  get -e CHECKSUMS.md5
  get -e CHECKSUMS.md5.asc
  mirror --delete chromium-ungoogled/build/ chromium-ungoogled/build/
EOF

gpg --verify CHECKSUMS.md5.asc

grep chromium-ungoogled/build CHECKSUMS.md5 | md5sum --check

patch -p0 chromium-ungoogled/build/chromium.SlackBuild <<EOF
diff --git a/chromium.SlackBuild.orig b/chromium.SlackBuild
old mode 100644
new mode 100755
index 7bcd3d0..aa52e7f
--- a/chromium.SlackBuild.orig
+++ b/chromium.SlackBuild
@@ -733,6 +733,9 @@ if [ ${USE_UNGOOGLED} -ne 0 ]; then
   #SRCURL[2]="https://github.com/tangalbert919/ungoogled-chromium/archive/${UNGOOGLED}.tar.gz"
 fi
 
+SOURCE[3]="${SOURCE[0]}.hashes"
+SRCURL[3]="${SRCURL[0]}.hashes"
+
 # Create working directories:
 mkdir -p $OUTPUT          # place for the package to be saved
 mkdir -p $TMP/tmp-$PRGNAM # location to build the source
@@ -751,7 +754,7 @@ for (( i = 0; i < ${#SOURCE[*]}; i++ )) ; do
     if [ -f ${SOURCE[$i]} ]; then echo "Ah, found it!"; continue; fi
     if ! [ "x${SRCURL[$i]}" == "x" ]; then
       echo "Will download file to $(dirname $SOURCE[$i])"
-      wget --no-check-certificate -nv -T 20 -O "${SOURCE[$i]}" "${SRCURL[$i]}" || true
+      wget -nv -T 20 -O "${SOURCE[$i]}" "${SRCURL[$i]}" || true
       if [ $? -ne 0 -o ! -s "${SOURCE[$i]}" ]; then
         echo "Fail to download '$(basename ${SOURCE[$i]})'. Aborting the build."
         mv -f "${SOURCE[$i]}" "${SOURCE[$i]}".FAIL
@@ -769,6 +772,17 @@ if [ "$P1" == "--download" ]; then
   exit 0
 fi
 
+# Verify file integrity:
+echo "Verifying chromium source tarball integrity..."
+sed -e '/^sha512/!d; s/^sha512\s*//' -e "s/chromium/$(sed "s;/;\\\/;g" <<<"$SRCDIR")\/chromium/" ${SOURCE[3]} | sha512sum --check 2>&1 > /dev/null
+
+if [ $? -ne 0 ]; then
+  echo "Failed to verify ${SOURCE[0]} checksum. Aborting the build."
+  exit 1
+fi
+
+echo "Verification done."
+
 # --- PACKAGE BUILDING ---
 
 echo "++"
@@ -783,28 +797,6 @@ if [ ${USE_UNGOOGLED} -ne 0 ]; then
 fi
 cd ${SRCNAM}-${VERSION}
 
-if [ ${USE_UNGOOGLED} -ne 0 ]; then
-  # Give chromium-ungoogled our own Slackware grey logo:
-  for size in 16 32 ; do
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/default_200_percent/chromium/product_logo_$size.png
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/default_100_percent/chromium/product_logo_$size.png
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/default_100_percent/chromium/linux/product_logo_$size.png
-  done
-  for size in 24 48 64 128 256; do
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/chromium/product_logo_$size.png
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/chromium/linux/product_logo_$size.png
-  done
-  for size in 32 ; do
-    convert ${SRCDIR}/${PRGNAM}.png -resize ${size}x${size} \
-      chrome/app/theme/chromium/linux/product_logo_$size.xpm
-  done
-fi
-
 if [ ${USE_SYSROOT} -eq 1 ]; then
   # Download a sysroot image:
   python3 build/linux/sysroot_scripts/install-sysroot.py --arch=${SYSROOT_ARCH}
@@ -1208,6 +1200,9 @@ if [ ${USE_CLANG} -eq 1 ] && [  ${BUILD_CLANG} -eq 1 ]; then
       fi
     done
   fi
+
+  sed -i "/\s*base_cmake_args =/a '-DLLVM_FORCE_USE_OLD_TOOLCHAIN=ON'," tools/clang/scripts/build.py
+
   LDFLAGS="$LDFLAGS" \
   python3 tools/clang/scripts/build.py \
     --bootstrap \
EOF

exit 0
