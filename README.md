Snippets
========

Just an assorted collection of snippets & scripts.

**Warning! Legacy software ahead! ;)**

`android`
* `afwall-startup.sh`: An AFWall+ firewall based on the one by [Mike Kuketz](https://www.kuketz-blog.de/afwall-wie-ich-persoenlich-die-android-firewall-nutze/).
For more information on custom scripts, see the [AFWall+ Wiki](https://github.com/ukanth/afwall/wiki/CustomScripts).
* `debloat-*.sh`: Debloating scripts for various android devices.

`misc`
* `ternary-phase-diagram.R`: A demonstration of a ternary phase diagram written in R.

`qubes`
* `qubes-firewall-user-script`: A minimalstic firewall for Qubes OS. More info is
available in the [official user documentation](https://www.qubes-os.org/doc/firewall/).

`scripts`
* `img4web.sh`: Compress PNG & JPG images for web.
* `libgen-check.sh`: Check local books for availability at Library Genesis.
* `tosec-bat2sh.sh`: Convert TOSEC `.bat` files to `.sh`.
* `tosec-unmerge.sh`: Unmerge TOSEC collections... nothing more.
* `wine-cleanup.sh`: Wine is (was?) notorious for spilling all over the place...
* `wv2flac.sh`: Convert wavpack-encoded audio files to FLAC en masse and update
the corresponding .cue files. Could be adapted to any other format pair, duh!
* `xfce-setup.sh`: A tiny bootstrap for the Xfce.

`slackware`
* `get-chromium-ungoogled.sh`: Downloads the latest `chromium-ungoogled.SlackBuild`
from AliienBob's FTP, checks MD5, and patches SlackBuild.
