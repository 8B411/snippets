#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

set -e

# https://wiki.winehq.org/FAQ#How_do_I_clean_the_Open_With_List.3F
rm -f ~/.local/share/mime/packages/x-wine*
rm -f ~/.local/share/applications/wine-extension*
rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
rm -f ~/.local/share/mime/application/x-wine-extension* 

# https://forum.xfce.org/viewtopic.php?pid=30662#p30662
cp -a /usr/share/applications/wine.desktop ~/.local/share/applications
echo "Hidden=true" >> ~/.local/share/applications/wine.desktop
update-desktop-database ~/.local/share/applications

exit 0