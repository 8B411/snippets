#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

find . -type f -iname "*.wv" -print0 | while IFS= read -r -d '' file; do
  cd "${file%/*}"
  wvunpack "$file" \
    && flac -8 "${file%.wv}.wav" \
    && rm "${file%.wv}.wav"
  sed -i "/^FILE/ s/\.wv/.flac/" "$(find . -type f -iname "*.cue")"
done

exit 0