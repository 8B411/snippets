#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2023 8B411 <8B411@disroot.org>

set -e

find . -type f -regex ".*(TOSEC-v[0-9]+-[0-9]+-[0-9]+_CM)\.zip" \
  \( -execdir unzip {} \; -and -delete \)

exit 0
