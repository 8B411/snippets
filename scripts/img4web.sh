#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

find . -type f -iregex ".*\.\(png\|jpe?g\)$" -printf '%h\0' | uniq -z | while IFS= read -r -d '' dir; do
  pushd "$dir"
  mkdir web
  find -type f -iregex ".*\.\(png\|jpe?g\)$" -exec mogrify \
    -define jpeg:dct-method=float \
    -format jpg \
    -interlace Plane \
    -path web \
    -quality 85% \
    -sampling-factor 4:2:0 \
    -strip \
    {} +
  popd
done