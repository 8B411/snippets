#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

find . -type f -iregex ".*\.\(azr\|djvu\|epub\|pdf\)$" -printf '%h\0' | while IFS= read -r -d '' file; do
  if [[ "$(curl -s "http://libgen.rs/json.php?ids=$(md5sum "$file")&fields=id")" = "[]" ]]; then
    echo "$file"
  fi
  sleep 5
done

exit 0