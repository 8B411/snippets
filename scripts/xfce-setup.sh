#!/bin/bash

# Do not roll up windows or scroll workspaces with mousewheel
# and remove rollup button from window title
xfconf-query -c xfwm4 -p /general/mousewheel_rollup -s false
xfconf-query -c xfwm4 -p /general/scroll_workspaces -s false
xfconf-query -c xfwm4 -p /general/button_layout -s "O|HMC"

# Remove suspend button from logout menu
xfconf-query -c xfce4-session -p /shutdown/ShowSuspend -n -t bool -s false

# Disable session management
xfconf-query -c xfce4-sessions -p /general/SaveOnExit -s false
find ~/.cache/sessions/ -mindepth 1 -delete
chmod -w ~/.cache/sessions

# Change icon size for HiDPI displays in Xfce 4.6+
# http://www.ceus-now.com/adapt-font-and-icon-sizes-to-high-definition-screen-resolutions-in-ubuntu-studio-xfce/
xfconf-query -c xsettings -p "/Gtk/IconSizes" -s "gtk-button=24,24:gtk-dialog=48,48:gtk-dnd=24,24:gtk-large-toolbar=24,24:gtk-menu=24,24:gtk-small-toolbar=16,16"
xfconf-query -c xsettings -p "/Gtk/CursorThemeSize" -s "32"
