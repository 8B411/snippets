#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2023 8B411 <8B411@disroot.org>

set -e

find . -type f -name "TOSEC*.bat" | while IFS= read -r file; do
  cat <<-EOF | sed -f - -i "$file"
    1 i\#!/bin/bash\n
    s/\r//g
    s:\\\:/:g
    s:':'\\\'':g
    s:\":':g
    s/^mkdir/mkdir -p/g
    s/^move/mv/g
EOF
  chmod u+x "$file"
  mv "$file" "${file%.*}.sh"
done

exit 0
