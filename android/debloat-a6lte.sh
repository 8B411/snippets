#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>
# Debloat Android 10 on Samsung Galaxy A6/A6+ (SM-A600FN/SM-A605FN)

set -x

bloatware=(
  com.android.vending
  com.facebook.appmanager
  com.facebook.katana
  com.facebook.services
  com.facebook.system
  com.google.android.apps.maps
  com.google.android.apps.restore
  com.google.android.apps.tachyon
  com.google.android.apps.turbo
  com.google.android.configupdater
  com.google.android.feedback
  com.google.android.gm
  com.google.android.gms
  com.google.android.gms.location.history
  com.google.android.googlequicksearchbox
  com.google.android.gsf
  com.google.android.onetimeinitializer
  com.google.android.overlay.gmsconfig
  com.google.android.overlay.gmsgsaconfig
  com.google.android.partnersetup
  com.google.android.printservice.recommendation
  com.google.android.projection.gearhead
  com.google.android.setupwizard
  com.google.android.syncadapters.calendar
  com.google.android.syncadapters.contacts
  com.google.android.tts
  com.google.android.youtube
  com.linkedin.android
  com.microsoft.office.excel
  com.microsoft.office.powerpoint
  com.microsoft.office.word
  com.microsoft.skydrive
  flipboard.boxer.app
)

for i in ${bloatware[@]}; do
  adb shell "pm disable-user --user 0 $i"
done

exit 0
