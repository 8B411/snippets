#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024 8B411 <8B411@disroot.org>
# Debloat Android 8 on Samsung Galaxy A7 (2017) (SM-A720F,SM-A720F/DS)

set -x

bloatware=(
  com.android.vending
  com.facebook.appmanager
  com.facebook.katana
  com.facebook.system
  com.google.android.apps.docs
  com.google.android.apps.maps
  com.google.android.apps.photos
  com.google.android.backuptransport
  com.google.android.configupdater
  com.google.android.feedback
  com.google.android.gm
  com.google.android.gms
  com.google.android.googlequicksearchbox
  com.google.android.gsf
  com.google.android.music
  com.google.android.onetimeinitializer
  com.google.android.partnersetup
  com.google.android.printservice.recommendation
  com.google.android.setupwizard
  com.google.android.syncadapters.calendar
  com.google.android.syncadapters.contacts
  com.google.android.talk
  com.google.android.tts
  com.google.android.videos
  com.google.android.webview
  com.google.android.youtube
  com.linkedin.android
  com.megogo.application
  com.microsoft.office.excel
  com.microsoft.office.powerpoint
  com.microsoft.office.word
  com.microsoft.skydrive
  flipboard.boxer.app
  net.megogo.vendor

  com.dsi.ant.plugins.antplus
  com.dsi.ant.sample.acquirechannels
  com.dsi.ant.server
  com.dsi.ant.service.socket
  com.samsung.android.app.notes
  com.samsung.android.email.provider
  com.samsung.android.voc
  com.sec.android.app.myfiles
  com.sec.android.app.popupcalculator
  com.sec.android.app.samsungapps
  com.sec.android.app.sbrowser
  com.sec.android.app.shealth
  com.sec.android.app.voicenote
  com.sec.android.gallery3d
  com.sec.spp.push
)

for i in ${bloatware[@]}; do
  adb shell "pm disable-user --user 0 $i"
done

exit 0
