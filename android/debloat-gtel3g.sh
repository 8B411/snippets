#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>
# Debloat Android 4.4.4 on Samsung Galaxy Tab E (SM-T561)

set -x

bloatware=(
  com.android.vending
  com.google.android.apps.books
  com.google.android.apps.docs
  com.google.android.apps.magazines
  com.google.android.apps.maps
  com.google.android.apps.plus
  com.google.android.backuptransport
  com.google.android.configupdater
  com.google.android.feedback
  com.google.android.gm
  com.google.android.gms
  com.google.android.googlequicksearchbox
  com.google.android.gsf
  com.google.android.gsf.login
  com.google.android.marvin.talkback
  com.google.android.music
  com.google.android.onetimeinitializer
  com.google.android.partnersetup
  com.google.android.play.games
  com.google.android.setupwizard
  com.google.android.street
  com.google.android.syncadapters.calendar
  com.google.android.syncadapters.contacts
  com.google.android.talk
  com.google.android.tts
  com.google.android.videos
  com.google.android.youtube
  com.hancom.androidpc.appwidget
  com.hancom.androidpc.hanupdater
  com.hancom.androidpc.launcher.shared
  com.hancom.androidpc.viewer.launcher
  com.hancom.office.hcell.viewer.hcell_viewer_apk
  com.hancom.office.hshow.viewer.hshow_viewer_apk
  com.hancom.office.hword.viewer.hword_apk
)

for i in ${bloatware[@]}; do
  adb shell "su -c 'pm disable $i'"
done

exit 0
