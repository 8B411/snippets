#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>
# Debloat Android 8 on Samsung Galaxy Tab A (SM-T585)

set -x

bloatware=(
  com.android.vending
  com.google.android.apps.docs
  com.google.android.apps.maps
  com.google.android.apps.photos
  com.google.android.backuptransport
  com.google.android.configupdater
  com.google.android.feedback
  com.google.android.gm
  com.google.android.gms
  com.google.android.googlequicksearchbox
  com.google.android.gsf
  com.google.android.music
  com.google.android.onetimeinitializer
  com.google.android.partnersetup
  com.google.android.printservice.recommendation
  com.google.android.setupwizard
  com.google.android.syncadapters.calendar
  com.google.android.syncadapters.contacts
  com.google.android.talk
  com.google.android.tts
  com.google.android.videos
  com.google.android.webview
  com.google.android.youtube
  com.megogo.application
  com.microsoft.office.excel
  com.microsoft.office.onenote
  com.microsoft.office.powerpoint
  com.microsoft.office.word
  com.microsoft.skydrive
  com.skype.raider
  flipboard.boxer.app
  net.megogo.vendor
)

for i in ${bloatware[@]}; do
  adb shell "pm disable-user --user 0 $i"
done

exit 0
