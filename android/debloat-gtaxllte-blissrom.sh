#!/bin/bash
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>
# Debloat BlissRom v12.12 on Samsung Galaxy Tab A (SM-T585)

set -x

bloatware=(
  co.potatoproject.plugin.volume.aosp
  co.potatoproject.plugin.volume.compact
  co.potatoproject.plugin.volume.oreo
  co.potatoproject.plugin.volume.tiled
  com.android.chrome
  com.android.vending
  com.asus.stitchimage
  com.asus.stitchimage.overlay
  com.asus.stitchimage.service
  com.benzo.weathericons
  com.blissroms.aboutbliss
  com.blissroms.blisspapers
  com.blissroms.blissstats
  com.farmerbb.taskbar
  com.farmerbb.taskbar.support
  com.gnonymous.gvisualmod.sbh_l
  com.gnonymous.gvisualmod.sbh_m
  com.gnonymous.gvisualmod.sbh_xl
  com.gnonymous.gvisualmod.urm_l
  com.gnonymous.gvisualmod.urm_m
  com.gnonymous.gvisualmod.urm_r
  com.gnonymous.gvisualmod.urm_s
  com.google.android.apps.pixelmigrate
  com.google.android.apps.recorder
  com.google.android.apps.turbo
  com.google.android.apps.wallpaper.nexus
  com.google.android.apps.wellbeing
  com.google.android.configupdater
  com.google.android.contacts
  com.google.android.feedback
  com.google.android.gms
  com.google.android.gms.location.history
  com.google.android.gms.policy_sidecar_aps
  com.google.android.googlequicksearchbox
  com.google.android.googlequicksearchbox.nga_resources
  com.google.android.gsf
  com.google.android.inputmethod.latin
  com.google.android.modulemetadata
  com.google.android.onetimeinitializer
  com.google.android.partnersetup
  com.google.android.pixel.setupwizard
  com.google.android.pixel.setupwizard.overlay
  com.google.android.pixel.setupwizard.overlay.aod
  com.google.android.printservice.recommendation
  com.google.android.settings.intelligence
  com.google.android.setupwizard
  com.google.android.soundpicker
  com.google.android.syncadapters.contacts
  com.google.android.webview
  com.motorola.faceunlock
  com.motorola.faceunlock.overlay
  com.oneplus.screenrecord
  org.omnirom.omnijaws
  org.omnirom.omnistyle
)

for i in ${bloatware[@]}; do
  adb shell "pm disable-user --user 0 $i"
done

exit 0
