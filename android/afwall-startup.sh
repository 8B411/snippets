# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2022 8B411 <8B411@disroot.org>

# Disable IPv6
echo 0 > /proc/sys/net/ipv6/conf/wlan0/accept_ra || true
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6
echo 2 > /proc/sys/net/ipv6/conf/all/use_tempaddr
echo 2 > /proc/sys/net/ipv6/conf/default/use_tempaddr

# Disable captive portal
settings put global captive_portal_detection_enabled 0
settings put global captive_portal_mode 0

# Set default NTP server
settings put global ntp_server pool.ntp.org

iptables=/system/bin/iptables
ip6tables=/system/bin/ip6tables

# Set default IPv4 policy to DROP
$iptables -F INPUT
$iptables -F FORWARD
$iptables -t nat -F
$iptables -t mangle -F
$iptables -t raw -F
$iptables -X || true
$iptables -P INPUT DROP
$iptables -P FORWARD DROP
$iptables -P OUTPUT DROP

# Set default IPv6 policy to DROP
$ip6tables -F INPUT
$ip6tables -F FORWARD
$ip6tables -t nat -F || true
$ip6tables -t mangle -F
$ip6tables -t raw -F
$ip6tables -X || true
$ip6tables -P INPUT DROP
$ip6tables -P FORWARD DROP
$ip6tables -P OUTPUT DROP

# Block Google DNS
$iptables -A afwall -d 8.8.8.8 -j DROP
$iptables -A afwall -d 8.8.4.4 -j DROP

# Allow incoming traffic from established connections
$iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
# Allow DNS-over-HTTPS (optional)
# $iptables -A afwall -p tcp --dport 853 -m owner --uid-owner 0 -j ACCEPT

# Allow incoming traffic from loopback
$iptables -A INPUT -i lo -j ACCEPT
$iptables -A afwall -o lo -j ACCEPT
